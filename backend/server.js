const express = require('express')
const mongoose = require('mongoose')
const app = express()
const cors = require('cors')
const bodyParser = require('body-parser')
const userRoute = require('./routes/User')
require('dotenv').config()

app.use(cors())
app.use(bodyParser.json())

mongoose.set('strictQuery', true);
mongoose.connect('mongodb://127.0.0.1:27017/quizapp_db',{
    useNewUrlParser: true,
    useUnifiedTopology: true
});
const db = mongoose.connection;
db.on('error', (error) => console.log(error));
db.once('open', () => console.log('Databases Connected...'));

app.use('/users', userRoute)

app.listen(5000, () => {
    console.log('Server started on 5000')
})



