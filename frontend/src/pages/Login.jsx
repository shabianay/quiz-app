import Navbar from "../components/Navbar";
import styled from 'styled-components'
import { useState } from 'react'
import { useNavigate } from "react-router-dom";
import axios from 'axios'
import { useDispatch } from "react-redux";
import { login } from "../redux/apiCalls";

const Container = styled.div`
width:100%;
height:100vh;
display: flex;
align-items: center;
justify-content: center;
background-color:whitesmoke;
overflow:hidden;
`
const Wrapper = styled.div`
width:60%;    
padding:20 px; 
border-radius:10px;
align-items: center;
justify-content: center;
background-color:#ffffff;
`
const HeroTitle = styled.h1`
font-size:32px;
font-weight:500;
text-align:center;
padding:20px;
border-bottom: 1px solid rgba(0,0,0,.25);
`
const Form = styled.form`
display: flex;
flex-wrap: wrap;
padding: 80px;
flex-direction: row-reverse;
align-items: center;
align-content: center;
justify-content: center;
flex-wrap: wrap;
`
const Input = styled.input`
flex:1;
margin-top: 20px;
padding:8px;
`
const Button = styled.button`
width: 100%;
border:none;
border-radius:15px;
padding: 10px 20px;
background-color:#2196F3;
color:white;
cursor:pointer;
margin-top: 60px;
`
const FormWrapper = styled.div`
width:40%;
margin:auto;
display: flex;
align-items: center;
justify-content:center;
flex-wrap: wrap;
`
const Link = styled.a`
margin-top: 50px;
font-size:15px;
text-decoration:underline;
cursor:pointer;
align-items: center;
justify-content: center;
`

const Login = () => {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate()
  const dispatch = useDispatch();

  const handleLogin = (e) => {
    e.preventDefault();
    const userCheck = {
      email: email,
      password: password,
    };
    try {
      axios.post("http://localhost:5000/users/login", userCheck).then((response) => {
        console.log(response.status);
        console.log(response.data);
        login(dispatch, { email, password });
        console.log("data suc")
        navigate("/Dashboard")
      });
    } catch {
      alert("wrong username and/or password")
    }
  };

  return (
    <>
      <Navbar />
      <Container>
        <Wrapper>
          <HeroTitle>Login</HeroTitle>
          <FormWrapper>
            <Form onSubmit={handleLogin}>
              <Input placeholder="Email" type="email" onChange={e => setEmail(e.target.value)} required />
              <Input placeholder="Password" type="password" onChange={e => setPassword(e.target.value)} required />
              <Button type="submit">Login</Button>
              <Link href="/register">Create a new account</Link>
            </Form>
          </FormWrapper>
        </Wrapper>
      </Container>
    </>
  )
}

export default Login