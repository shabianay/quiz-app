import Navbar from "../components/Navbar";
import styled from 'styled-components'
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from 'axios'

const Container = styled.div`
width:100%;
height:100vh;
display: flex;
align-items: center;
justify-content: center;
background-color:whitesmoke;
overflow:hidden;
`
const Wrapper = styled.div`
width:60%;    
padding:20px; 
border-radius:10px;
align-items: center;
justify-content: center;
background-color:#ffffff;
`
const HeroTitle = styled.h1`
font-size:32px;
font-weight:500;
text-align:center;
padding-top: 25px;
`
const Title = styled.h3`
font-size:18px;
font-weight:300;
text-align:center;
padding-bottom: 25px;
border-bottom: 1px solid rgba(0,0,0,.25);
`

const Form = styled.form`
display: flex;
flex-wrap: wrap;
padding: 80px;
`
const Input = styled.input`
flex:1;
margin:20px 10px 0px 0px;
padding:8px;
`
const Button = styled.button`
width: 100%;
border:none;
border-radius:15px;
padding: 10px 20px;
background-color:#2196F3;
color:white;
cursor:pointer;
margin-top: 60px;
margin-bottom: 60px;
`
const FormWrapper = styled.div`
width:40%;
margin:auto;
display: flex;
align-items: center;
justify-content:center;
flex-wrap: wrap;
`

const Register = () => {

  const [email, setEmail] = useState();
  const [name, setName] = useState();
  const [lastName, setLastName] = useState();
  const [pass, setPass] = useState();
  const navigate = useNavigate()

  const handleRegister = async (e) => {
    e.preventDefault();
    const registered = {
      firstname: name,
      lastname: lastName,
      email: email,
      password: pass,
    }
    console.log(registered)
    axios.post("http://localhost:5000/users/", registered).then((response) => {
      console.log(response.status);
      console.log(response.data);
    });

    navigate("/login");

  }


  return (
    <>
      <Navbar />
      <Container>
        <Wrapper>
          <HeroTitle>Sign up to QUIZ-APP</HeroTitle>
          <FormWrapper>
            <Form onSubmit={handleRegister}>
              <Input placeholder="Email" type="email" onChange={e => setEmail(e.target.value)} required />
              <Input placeholder="First Name" type="text" onChange={e => setName(e.target.value)} required />
              <Input placeholder="Last Name" type="text" onChange={e => setLastName(e.target.value)} required />
              <Input placeholder="Password" type="password" onChange={e => setPass(e.target.value)} required />
              <Button type="submit">Register</Button>
            </Form>
          </FormWrapper>
        </Wrapper>
      </Container>
    </>
  )
}

export default Register