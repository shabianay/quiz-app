import React from "react";
import {useNavigate} from "react-router-dom";
import Grid from '@mui/material/Grid';
import LoginNavbar from "../components/LoginNavbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";

const Dashboard = () => {
  let navigate = useNavigate();
  return (
    <div>
    <LoginNavbar />
      <Typography variant="h6" component="div" style={{marginTop:50, paddingTop: 50, fontWeight:600, fontSize:32, textAlign:'center',}}>
        QUIZ EARTH
      </Typography>
      <Grid container justifyContent="center">
      <div component="span" style={{padding: 50, fontWeight:600, fontSize:32, textAlign:'center',}}>
        <Button variant="contained" color="error" onClick={() => navigate('/Quiz')}>
          Quiz
        </Button>
        <Button variant="contained" color="error" onClick={() => navigate('/Learn')}>
          Learn
        </Button>
      </div>            
      </Grid> 
    </div>
  );
};
export default Dashboard;