import styled from 'styled-components'
import { Link } from "react-router-dom";
import { Logout } from '@mui/icons-material';
import { useDispatch } from "react-redux";
import { logoutD } from "../redux/apiCalls";

const Container = styled.div`
height:60px;
background-color: white;
`
const Wrapper = styled.div`
padding:0px 20px;
display:flex;
align-items:center;
justify-content:space-between
`
const Left = styled.div`
flex: 1;
display: flex;
align-items: center;
`
const Logo = styled.div`
font-size:32px;
font-weight:600;
color:#2196F3;
padding:10px 12px;
`
const MenuItem = styled.button`
font-size: 20px;
font-weight:600;
cursor: pointer;
color:#00000;
background-color:white;
padding:20px 35px;
border:none;
`
const Right = styled.div`
flex:1;
display:flex;
align-items: center;
justify-content:flex-end;
`
const MenuItemFirst = styled.div`
font-weight:600;
margin-left:25px;
margin-right: 50px;
cursor: pointer;
background-color:#2196F3;
color:#EEEEEE;
padding:10px 20px;
border-radius:5px;
border-color: #2196F3;
`

const LoginNavbar = () => {

    const dispatch = useDispatch();

    const handleLogout = () => {
        logoutD(dispatch)
    }

    return (
        <Container>
            <Wrapper>
                <Left>
                    <Logo>QUIZ-APP</Logo>
                    <Link to="/dashboard">
                        <MenuItem>Dashboard</MenuItem>
                    </Link>
                </Left>
                <Right>
                    <Link to="/" style={{ textDecoration: "none"}}>
                        <MenuItemFirst><Logout style={{ verticalAlign: "middle" }} onClick={handleLogout} /> Logout</MenuItemFirst>
                    </Link>
                </Right>
            </Wrapper>
        </Container>
    )
}

export default LoginNavbar