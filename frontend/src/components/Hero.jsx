import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom';

const Container = styled.div`
width:100%;
height:100vh;
display:flex;
position:relative;
overflow:hidden; 
`

const Wrapper = styled.div`
height:100%;
display:flex;
`

const Slide = styled.div`
width:100vw;
height:100vh;
display:flex;
align-items:center;
background-color: #2196F3;
`

const ImgContainer = styled.div`
flex:1;
`

const Image = styled.img`
height:50%;
`

const Title = styled.h1`
font-size:70px;
color:#EEEEEE;
margin-left:5%;
`

const InfoContainer = styled.div`
flex:1;
padding:50px;
margin-bottom: 100px;
`;

const Button = styled.button`
    font-weight:600;
    background-color:#2196F3;
    border-radius: 5px;
    border-color: white;
    padding:15px;
    font-size:20px;
    cursor:pointer;
    margin-left:5%;
`

const Hero = () => {
    return (
        <Container>
            <Wrapper>
                <Slide>
                    <InfoContainer>
                        <Title>
                        Online test
                        </Title>
                        <Button>
                            <Link to="/register" style={{ textDecoration: "none", color:"#EEEEEE"}}>
                            Get Started for Free
                            </Link>
                        </Button>
                    </InfoContainer>
                    <ImgContainer>
                        <Image src="https://www.mastersoftwaresolutions.com/wp-content/uploads/2019/06/bnr-36.png" />
                    </ImgContainer>
                </Slide>
            </Wrapper>
        </Container>
    )
}

export default Hero