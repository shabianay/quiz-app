import React from 'react'
import styled from 'styled-components'
import { Link } from "react-router-dom";


const Container = styled.div`
height:60px;
background-color: white;
`

const Wrapper = styled.div`
padding:0px 20px;
display:flex;
align-items:center;
justify-content:space-between
`

const Left = styled.div`
flex: 1;
display: flex;
align-items: center;
`

const Logo = styled.div`
font-size:32px;
font-weight:600;
color:#2196F3;
margin-left:10%;
padding:10px 12px;
`

const MenuItem = styled.button`
font-size: 20px;
font-weight:600;
cursor: pointer;
color:#00000;
background-color:white;
padding:0 35px;
border:none;
`

const Right = styled.div`
flex:1;
display:flex;
align-items: center;
justify-content:flex-end;
`

const MenuItemFirst = styled.div`
font-weight:600;
margin-left:25px;
margin-right: 50px;
cursor: pointer;
background-color:#2196F3;
color:#EEEEEE;
padding:10px 20px;
border-radius:5px;
border-color: #2196F3;
`

const MenuItemSecond = styled.div`
border: 1px solid #2196F3;
font-weight:600;
background-color: white;
padding: 14px 28px;
font-size: 16px;
cursor: pointer;
margin-left:25px;
color: #2196F3;
padding:10px 20px;
border-radius:5px;
`


const Navbar = () => {
    return (
        <Container>
            <Wrapper>
                <Left>
                    <Logo>QUIZ-APP</Logo>
                    <Link to="/">
                        <MenuItem>Home</MenuItem>
                    </Link>
                </Left>
                <Right>
                    <Link to="/login" style={{ textDecoration: "none" }}>
                        <MenuItemSecond>Login</MenuItemSecond>
                    </Link>
                    <Link to="/register" style={{ textDecoration: "none" }}>
                        <MenuItemFirst>Sign up</MenuItemFirst>
                    </Link>
                </Right>
            </Wrapper>
        </Container>
    )
}

export default Navbar