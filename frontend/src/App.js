import Home from "./pages/Home";
import React from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import Login from "./pages/Login";
import Register from "./pages/Register";
import Dashboard from "./pages/Dashboard";
import { useSelector } from "react-redux";
import Quiz from "./components/Quiz";
import Learn from "./components/Learn";

function App() {
  var currentUserUid;

  const userId = useSelector((state) => state.user.currentUser);
  if (userId == null) {
    console.log("no auth")
  } else {
    currentUserUid = userId.user._id
  }

  const RequireAuth = ({ children }) => {
    return currentUserUid ? (children) : <Navigate to="/login" />
  }

  return (
    <div>
      <Routes>
        <Route path="/" element={<App />}></Route>
        <Route index element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/dashboard" element={<RequireAuth><Dashboard CUId={currentUserUid} /></RequireAuth>} />
        <Route path="/quiz" element={<Quiz />} />
        <Route path="/learn" element={<Learn />} />
      </Routes>
    </div>
  );
}

export default App;
